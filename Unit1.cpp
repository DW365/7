//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
        Form2->Show();
        Form1->ActiveControl = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
        int step = 10;
        Beep(440,200);
        if(Key == VK_LEFT) Shape1->Left-=step;
        if(Key == VK_RIGHT) Shape1->Left+=step;
        if(Key == VK_UP) Shape1->Top-=step;
        if(Key == VK_DOWN) Shape1->Top+=step;
        Label3->Caption = IntToStr(Shape1->Left);
        Label4->Caption = IntToStr(Shape1->Top);
        Shape1->Update();
        Label3->Update();
        Label4->Update();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
        Label3->Caption = IntToStr(Shape1->Left);
        Label4->Caption = IntToStr(Shape1->Top);
}
//---------------------------------------------------------------------------
